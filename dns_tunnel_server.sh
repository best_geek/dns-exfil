#!/bin/bash


#please note this is is using pihole log format as a DNS Server

LOG="docker exec -it pihole tail -f /var/log/pihole.log"
TLD="randomdomain.net"
RECIEVED_DIR="/tmp"



echo "Starting DNS log searcher"
echo "Please be patient"

$LOG | grep $TLD | grep 'query' | grep 'from' | while read -r LINE

do
        #get query pos in log
        #strip TLD
        STRP=$(echo $LINE | awk {'print $6'} )
        STRP=$(echo $STRP | sed "s/.$TLD//") #get rid of our matching TLD


        #start looking for our start, middle and end of file
        if ( echo $STRP | grep -q '*STRT*' )
        then

                ##file start actions

                echo "File start was detected"
                FILESTART="True"
                FILE_VAR=""

                #make log file
                FILENAME="${RECIEVED_DIR}/$(date +%s).dnstunneler.log"
                touch $FILENAME
                echo "logs were stored in: $FILENAME"

                FILE_VAR=$STRP


        #if we've started the file and are recieving data bits
        elif  (echo $STRP | grep -q '*DAT*') && (echo $FILESTART | grep -q "True")
        then

                FILE_VAR=${FILE_VAR}${STRP}


        #if we've finished the file
        elif  (echo $STRP | grep -q '*FNSH*') && (echo $FILESTART | grep -q "True")
        then

                FILE_VAR=${FILE_VAR}${STRP}

                echo "date was captures on $(date)" > $FILENAME
                #get rid our custom headers and place in file
                echo $FILE_VAR >> $FILENAME.raw
                echo $FILE_VAR | sed 's/\*FNSH\*//g' | sed 's/\*DAT\*//g' | sed 's/\*STRT\*//g' | xxd -plain -revert >> $FILENAME
                echo "done convertion"
                echo "ending log for $FILENAME ... please wait a few seconds for it to become available. If it is a big file, this will take time"


        fi

done