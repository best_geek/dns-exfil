# DNS tunelling script and listener #

This repo provides two scripts which demonstrates a basic implementation of how an attacker can exfiltrate data out of a network using DNS and a custom DNS server



### dns_tunnel_server.sh ###

This was scripted to work by reciveing data through searching DNS logs generated by pihole. 
Pihole uses dnsmasq as the DNS provider, so in theory should work with any dnsmasq sever

The idea is that the script looks for a unique top level domain. You can make this as unique or as ovbious as you like

For example setting ```TLD="randomdomains123.net"``` would catch a DNS request for ```jczxklcjzl.randomdomains123```
The advanatage of putting a legitimate top level domain is that some tools may see this is legitimate (you could just as easily set it to facebook.com)
Of course, if you have any DGA detection or domain fluxing rules it may show up right away. 

Change the ```LOG``` variable to the appropriate command to get the logs. In the provided example, this simply scrapes them from a phihole docker image
Change the ```TLD``` variable to what you'd like to be the base identifier for a DNS exfil request
Change the ```RECIEVED_DIR``` variable to where you'd like the pieced together files to be stored. Once a file is identified, it is timestamped in this dir. 

Then proceed to run the server with 
sudo ./dns_tunnel_server.sh



### dns_tunnel_sender.sh ###

this script should sit on the machine you want to exfil data from. The script takes the file you want to send as the 1st argument. 

There are some vars that can be tweaked at the top
```

#chunks to divide the file into.Do not set higher than 20, you'll likely make an illegally sized DNS request.
MAX_LENGTH=20

# the destination for the listening dns server
DNS=192.168.0.10

#this should match the other script
TLD="randomdomains123.net"

```

There is a requirement to have dig installed so you can point your DNS request to a custom nameserver. You could change this to nslookup commands but you would have to find a way of getting into ```/etc/resolv.conf``` and changing the DNS server there.

Once you've done your tweaking, run the script. 

Each '.' on screen represents a DNS request. You should only use this script for small txt files, I did manage to exfil a powerpoint file but it took hours. 

### Basic understanding how how this works ###

* The file is broken down into chunks on the client side
* The data is encoded with xxd, which provides obsecurity in the DNS request and makes it hard for any network monitoring to actually identify the contents of the file. 
* Data either gets prefixed with STRT, DAT or FNSH to indicate to the server what pointin the file stream the client is at
* The server looks for the unique top level doman, and those prefixes and starts creating a new file 

### Why?  ###

It should provide a very basic example for how DNS tunelling can work. It's also a solid test as it should trip alarm bells if the appropriate monitoring is in place. 

